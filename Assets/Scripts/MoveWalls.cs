﻿using UnityEngine;
using System.Collections;

public class MoveWalls : MonoBehaviour {

	public GameObject wallLeft;
	public GameObject wallRight;

	float MoveSpeed;
	float ExitMoveSpeed;
	bool GoRight;
	bool GoLeft;


	float TimpStart;
	// Use this for initialization
	void Start () {
		TimpStart = Time.time;
		GoRight = true;
		GoLeft = true;
		MoveSpeed=Random.Range(5,30);
		MoveSpeed = MoveSpeed/10;
		ExitMoveSpeed = 2.2f; 

		TimpStart = Time.time;

	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time - TimpStart > 2)
		{
			ObstacleMove();
		}
	}


	void ObstacleMove()
	{

		if(!GamePlay.GameOver)
		{
		if(wallLeft.transform.position.x <= 0 && GoRight)
		{
			wallLeft.transform.position = new Vector3(wallLeft.transform.position.x + (MoveSpeed * Time.deltaTime),wallLeft.transform.position.y,wallLeft.transform.position.z);
		}
		
		if(this.transform.position.x >= -1.5f && !GoRight)
		{
			wallLeft.transform.position = new Vector3(wallLeft.transform.position.x - (MoveSpeed * Time.deltaTime),wallLeft.transform.position.y,wallLeft.transform.position.z);
		}
		
		if(wallLeft.transform.position.x <= -1.5f)
			GoRight = true;
		if(wallLeft.transform.position.x >= 0f)
			GoRight = false;
		
		
		
		
		
		
		
		
		if(wallRight.transform.position.x >= 0 && GoLeft)
		{
			wallRight.transform.position = new Vector3(wallRight.transform.position.x - (MoveSpeed * Time.deltaTime),wallRight.transform.position.y,wallRight.transform.position.z);
		}
		
		if(wallRight.transform.position.x <= 1.5f && !GoLeft)
		{
			wallRight.transform.position = new Vector3(wallRight.transform.position.x + (MoveSpeed * Time.deltaTime),wallRight.transform.position.y,wallRight.transform.position.z);
		}
		
		
		if(wallRight.transform.position.x >= 1.5f )
			GoLeft = true;
		if(wallRight.transform.position.x <= 0f)
			GoLeft = false;
		}
		else
		{
			wallRight.transform.position = new Vector3(wallRight.transform.position.x + (ExitMoveSpeed * Time.deltaTime),wallRight.transform.position.y,wallRight.transform.position.z);
			wallLeft.transform.position = new Vector3(wallLeft.transform.position.x - (ExitMoveSpeed * Time.deltaTime),wallLeft.transform.position.y,wallLeft.transform.position.z);

		}
	}


}
