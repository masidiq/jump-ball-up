﻿using UnityEngine;
using System.Collections;
using StartApp;

public class GameOverShow : MonoBehaviour {

	// Use this for initialization
	void Start () {
		#if UNITY_ANDROID
		StartAppWrapper.loadAd();
		#endif 

		PlayerPrefs.SetInt("ChartGameOver",PlayerPrefs.GetInt("ChartGameOver")+1);
	}
	
	// Update is called once per frame
	void Update () {
		if (PlayerPrefs.GetInt ("ChartGameOver") == 3) {
			PlayerPrefs.SetInt ("ChartGameOver", 0);
			StartAppWrapper.showAd();
		}
	}
}