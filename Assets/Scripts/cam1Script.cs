﻿using UnityEngine;
using System.Collections;

public class cam1Script : MonoBehaviour {
	float jiggleAmt=0.0f; // how much to shake
	Vector3 initial;
	// Use this for initialization
	void Start () {
		initial=this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	if(jiggleAmt>0) {
  float quakeAmt = Random.value*jiggleAmt*2 - jiggleAmt;
  Vector3 pp = transform.position;
  pp.y+= quakeAmt; // can also add to x and/or z
  transform.position = pp;
	
}
		
	
	}
	
	public void jiggleCam(float amt, float duration) {
  // Amt is how many meters the camera shakes. 0.5 is noticable
  jiggleAmt = amt;
  StartCoroutine(jiggleCam2(duration));
}
 
IEnumerator jiggleCam2(float duration) {
  yield return new WaitForSeconds(duration);
  jiggleAmt=0;
}
	
	public void RepositionCamera()
	{
		this.transform.position = initial;
	}
//	public static void Shake()
//	{
//		jiggleCam(0.5f,0.75f);
//	}
}
