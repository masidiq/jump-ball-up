﻿using UnityEngine;
using System.Collections;

public class ObstacleLeft : MonoBehaviour {

	bool GoRight;


	// Use this for initialization
	void Start () {
		GoRight = true;
	}
	
	// Update is called once per frame
	void Update () {


			if(this.transform.position.x <= 0 && GoRight)
			{
				this.transform.position = new Vector3(this.transform.position.x + (0.7f * Time.deltaTime),this.transform.position.y,this.transform.position.z);
			}

			if(this.transform.position.x >= -1.5f && !GoRight)
			{
				this.transform.position = new Vector3(this.transform.position.x - (0.7f * Time.deltaTime),this.transform.position.y,this.transform.position.z);
			}

			if(this.transform.position.x <= -1.5f)
				GoRight = true;
			if(this.transform.position.x >= 0f)
				GoRight = false;
	}
}
