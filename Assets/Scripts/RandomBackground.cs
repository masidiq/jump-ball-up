﻿using UnityEngine;
using System.Collections;

public class RandomBackground : MonoBehaviour {

	public Sprite[] Backgrounds;

	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt("Background",Random.Range(0,5));
		this.GetComponent<SpriteRenderer>().sprite = Backgrounds[PlayerPrefs.GetInt("Background")];
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
