﻿using UnityEngine;
using System.Collections;

public class ObstacleRight : MonoBehaviour {

	bool GoLeft;

	// Use this for initialization
	void Start () {
		GoLeft = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(this.transform.position.x >= 0 && GoLeft)
		{
			this.transform.position = new Vector3(this.transform.position.x - (0.7f * Time.deltaTime),this.transform.position.y,this.transform.position.z);
		}

		if(this.transform.position.x <= 1.5f && !GoLeft)
		{
			this.transform.position = new Vector3(this.transform.position.x + (0.7f * Time.deltaTime),this.transform.position.y,this.transform.position.z);
		}


		if(this.transform.position.x >= 1.5f)
			GoLeft = true;
		if(this.transform.position.x <= 0f)
			GoLeft = false;
	}
}
