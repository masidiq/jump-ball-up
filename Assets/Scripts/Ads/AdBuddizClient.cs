﻿using UnityEngine;

public class AdBuddizClient : MonoBehaviour {
	public static AdBuddizClient instance;

	void Awake(){
		if (instance == null) {
			instance = this;		
		}
	}
	void Start() // Init SDK on app start
	{ 
		AdBuddizBinding.SetLogLevel(AdBuddizBinding.ABLogLevel.Info);         // log level
		AdBuddizBinding.SetAndroidPublisherKey("c790933c-3092-4988-a340-c269b52f2a36"); // replace with your Android app publisher key
		AdBuddizBinding.SetIOSPublisherKey("TEST_PUBLISHER_KEY_IOS");         // replace with your iOS app publisher key
		//AdBuddizBinding.SetTestModeActive();                                  // to delete before submitting to store
		AdBuddizBinding.CacheAds();                                           // start caching ads
	}
	
	public void Show()
	{
		AdBuddizBinding.ShowAd ();
	}
	
	void OnEnable()
	{
		// Listen to AdBuddiz events
		AdBuddizManager.didFailToShowAd += DidFailToShowAd;
		AdBuddizManager.didCacheAd += DidCacheAd;
		AdBuddizManager.didShowAd += DidShowAd;
		AdBuddizManager.didClick += DidClick;
		AdBuddizManager.didHideAd += DidHideAd;
	}
	
	void OnDisable()
	{
		// Remove all event handlers
		AdBuddizManager.didFailToShowAd -= DidFailToShowAd;
		AdBuddizManager.didCacheAd -= DidCacheAd;
		AdBuddizManager.didShowAd -= DidShowAd;
		AdBuddizManager.didClick -= DidClick;
		AdBuddizManager.didHideAd -= DidHideAd;
	}
	
	void DidFailToShowAd(string adBuddizError) {
	//	AdBuddizBinding.LogNative("DidFailToShowAd: " + adBuddizError);
		//AdBuddizBinding.ToastNative("DidFailToShowAd: " + adBuddizError);
		Debug.Log ("Unity: DidFailToShowAd: " + adBuddizError);
	}
	
	void DidCacheAd() {
	//	AdBuddizBinding.LogNative ("DidCacheAd");
	//	AdBuddizBinding.ToastNative ("DidCacheAd");
		Debug.Log ("Unity: DidCacheAd");
	}
	
	void DidShowAd() {
	//	AdBuddizBinding.LogNative ("DidShowAd");
	//	AdBuddizBinding.ToastNative ("DidShowAd");
		Debug.Log ("Unity: DidShowAd");
	}
	
	void DidClick() {
	//	AdBuddizBinding.LogNative ("DidClick");
	//	AdBuddizBinding.ToastNative ("DidClick");
		Debug.Log ("Unity: DidClick");
	}
	
	void DidHideAd() {
	//	AdBuddizBinding.LogNative ("DidHideAd");
	//	AdBuddizBinding.ToastNative ("DidHideAd");
		Debug.Log ("Unity: DidHideAd");
	}
	
	void DidFetch() {
	//	AdBuddizBinding.LogNative ("DidFetch");
	//	AdBuddizBinding.ToastNative ("DidFetch");
		Debug.Log ("Unity: DidFetch");
	}
	
	void DidFail(string adBuddizError) {
	//	AdBuddizBinding.LogNative ("DidFail: " + adBuddizError);
	//	AdBuddizBinding.ToastNative ("DidFail: " + adBuddizError);
		Debug.Log ("Unity: DidFail: " + adBuddizError);
	}
	
	void DidComplete() {
		//AdBuddizBinding.LogNative ("DidComplete");
		//AdBuddizBinding.ToastNative ("DidComplete");
		Debug.Log ("Unity: DidComplete");
	}
	
	void DidNotComplete() {
	//	AdBuddizBinding.LogNative ("DidNotComplete");
	//	AdBuddizBinding.ToastNative ("DidNotComplete");
		Debug.Log ("Unity: DidNotComplete");
	}
}


