﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RevMobManager : MonoBehaviour, IRevMobListener {
	public static RevMobManager instance;
	private RevMob revmob;
	private RevMobPopup popup;

	private static readonly Dictionary<string, string> appIds = new Dictionary<string, string>() {
		{ "Android", "56717efd98fd81857c958919"}, // Find your RevMob App IDs in revmob.com
	};
	void Awake(){
		if (instance == null) {
			instance = this;
			revmob = RevMob.Start(appIds, "RevMob");
		}
	}

	void Start(){
		popup = revmob.CreatePopup();
	}
	public void Show(){
		if (popup != null) {
			Debug.Log ("MENAMPILKAN");
			popup.Show ();

		} else {
			Debug.Log ("LOAD POPUP");
			popup = revmob.CreatePopup();
		}
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)) { 
			//fullscreen.Hide();
		
		}
	}

	#region IRevMobListener implementation
	public void SessionIsStarted() {
		Debug.Log(">>> Session Started");
	}
	
	public void SessionNotStarted(string message) {
		Debug.Log(">>> Session not started");
	}
	
	public void AdDidReceive(string revMobAdType) {
		Debug.Log(">>> AdDidReceive: " + revMobAdType);
	}
	
	public void AdDidFail(string revMobAdType) {
		Debug.Log(">>> AdDidFail: " + revMobAdType);
	}
	
	public void AdDisplayed(string revMobAdType) {
		Debug.Log(">>> AdDisplayed: " + revMobAdType);
	}
	
	public void UserClickedInTheAd(string revMobAdType)	{
		Debug.Log(">>> AdClicked: " + revMobAdType);
	}
	
	public void UserClosedTheAd(string revMobAdType) {
		Debug.Log(">>> AdClosed: " + revMobAdType);
	}
	
	public void VideoLoaded() {
		Debug.Log(">>> VideoLoaded");
	}
	public void VideoNotCompletelyLoaded() {
		Debug.Log(">>> VideoNotCompletelyLoaded");
	}
	public void VideoStarted() {
		Debug.Log(">>> VideoStarted");
	}
	public void VideoFinished() {
		Debug.Log(">>> VideoFinished");
	}
	
	public void RewardedVideoLoaded() {
		Debug.Log(">>> RewardedVideoLoaded");
	}
	public void RewardedVideoNotCompletelyLoaded() {
		Debug.Log(">>> RewardedVideoNotCompletelyLoaded");
	}
	public void RewardedVideoStarted() {
		Debug.Log(">>> RewardedVideoStarted");
	}
	public void RewardedVideoFinished() {
		Debug.Log(">>> RewardedVideoFinished");
	}
	public void RewardedVideoCompleted() {
		Debug.Log(">>> RewardedVideoCompleted");
	}
	public void RewardedPreRollDisplayed() {
		Debug.Log(">>> RewardedPreRollDisplayed");
	}
	
	
	public void InstallDidReceive(string message) {}
	
	public void InstallDidFail(string message) {}
	
	public void EulaIsShown() {
		Debug.Log(">>> EulaShown");
	}
	
	public void EulaAccepted() {
		Debug.Log(">>> EulaAccepted");
	}
	
	public void EulaRejected() {
		Debug.Log(">>> EulaRejected");
	}
	
	#endregion
}
