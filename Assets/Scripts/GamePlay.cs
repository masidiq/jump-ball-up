﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Prime31;
using ChartboostSDK;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class GamePlay : MonoBehaviour {

//	private List<GameCenterLeaderboard> _leaderboards;
//	private List<GameCenterAchievementMetadata> _achievementMetadata;
//	private List<GameCenterPlayer> _friends;
//	
//	// bools used to hide/show different buttons based on what is loaded
//	private bool _hasFriends;
//	private bool _hasLeaderboardData;
//	private bool _hasAchievementData;




//	PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
//		// enables saving game progress.
//		.EnableSavedGames()
//			// registers a callback to handle game invitations received while the game is not running.
//			.WithInvitationDelegate(<callback method>)
//			// registers a callback for turn based match notifications received while the
//			// game is not running.
//			.WithMatchDelegate(<callback method>)
//			.Build();
	
//	PlayGamesPlatform.InitializeInstance(config);
	// recommended for debugging:
//	PlayGamesPlatform.DebugLogEnabled = true;
	// Activate the Google Play Games platform
//	PlayGamesPlatform.Activate();
	
	
	
	
	
	public string GameCenterID;
	public string AdColonyVersionID;
	public string AdColonyID;
	public string AdColonyZoneID;
	public string AdmobID;
	public string URLLink;

	Texture2D _logo;

	public Texture2D TapTap;

	public GameObject Obstacle1;
	public GameObject Obstacle2;
	public GameObject Obstacle3;
	public GameObject Ball;
	public GameObject Ground;
	public GameObject ColliderPocTrigger;

	public GUIStyle PlayButton;
	public GUIStyle TitleFont;
	public GUIStyle ScoreFont;
	public GUIStyle ScoreMainFont;
	public GUIStyle ReplayButton;
	public GUIStyle FreeGame;
	public GUIStyle Leader;
	public GUIStyle Like;
	public GUIStyle Share;

	public static bool GameOver;
	public static bool Playing;
	public static bool MoveCameraOnStart;

	public static float TimeDelayGameOver;
	public static int Scor;

	public AudioSource ButtonSource;
	AudioClip ButtonClip;
	public AudioSource LevelSource;
	AudioClip LevelClip;
	public AudioSource BallSource;
	AudioClip BallClip;
	public AudioSource GameOverSource;
	AudioClip GameOverClip;
	public AudioSource PocSource;
	AudioClip PocClip;


	public static bool PlayGameOverSound;
	public static bool PlayLevelSound;
	public static bool PlayPocSound;
	bool BooleanaMiscareODataGroundLaStart;
	bool AdMobApare;
//	GUITexture playbut;





	//FB

	protected string lastResponse = "";
	protected Texture2D lastResponseTexture;
	private void OnInitComplete()
	{
		Debug.Log("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);
	}
	
	
	
	void LoginCallback(FBResult result)
	{
		if (result.Error != null)
			lastResponse = "Error Response:\n" + result.Error;
		else if (!FB.IsLoggedIn)
		{
			lastResponse = "Login cancelled by Player";
		}
		else
		{
			lastResponse = "Login was successful!";
		}
	}
	
	
	private void OnHideUnity(bool isGameShown)
	{
		Debug.Log("Is game showing? " + isGameShown);
	}
	
	void FBlogin()
	{
		FB.Login("user_about_me, user_birthday", AuthCallback);
	}
	
	void AuthCallback(FBResult result)
	{
		if(FB.IsLoggedIn)
			Debug.Log("FB LOGIN WORKED");
		else
			Debug.Log("FAILED :(((");
	}
	
	private void CallFBActivateApp()
	{
		FB.ActivateApp();
		Callback(new FBResult("Check Insights section for your app in the App Dashboard under \"Mobile App Installs\""));
	}
	protected void Callback(FBResult result)
	{
		lastResponseTexture = null;
		// Some platforms return the empty string instead of null.
		if (!String.IsNullOrEmpty (result.Error))
		{
			lastResponse = "Error Response:\n" + result.Error;
		}
		else if (!String.IsNullOrEmpty (result.Text))
		{
			lastResponse = "Success Response:\n" + result.Text;
		}
		else if (result.Texture != null)
		{
			lastResponseTexture = result.Texture;
			lastResponse = "Success Response: texture\n";
		}
		else
		{
			lastResponse = "Empty Response\n";
		}
	}
	
	
	
	
	private IEnumerator TakeScreenshot()
	{
		yield return new WaitForEndOfFrame();
		
		var width = Screen.width;
		var height = Screen.height;
		var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
		// Read screen contents into the texture
		tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		tex.Apply();
		byte[] screenshot = tex.EncodeToPNG();
		
		var wwwForm = new WWWForm();
		wwwForm.AddBinaryData("image", screenshot, "InteractiveConsole.png");
		wwwForm.AddField("message", "Beat me at game #DashUp! Extremely addictive game. I can't stop playing it!");
		
		FB.API("me/photos", Facebook.HttpMethod.POST, Callback, wwwForm);
	}
	




	void Awake () {
		FB.Init(OnInitComplete, OnHideUnity);


		PlayGamesPlatform.DebugLogEnabled = true;
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate();





		LoadSounds();
		StartGameCenter();
		AdColony.Configure( AdColonyVersionID , AdColonyID , AdColonyZoneID );
		AdMobApare = true;
		Time.timeScale = 1.5f;
		FadeEffectGameOver = false;
		StergButonReplay = false;
		Playing = false;
		MoveCameraOnStart = true;
		GameOver = false;
		Scor=0;
		showtap = true;
//		StartCoroutine(FadeTime(2.0f));
		StartCoroutine(FadeStart(2.0f));
		i = 0.1f;
		moveDownSpeedGroundStart = 0.1f;
		BooleanaMiscareODataGroundLaStart = true;
		PlayLevelSound = false;
		PlayGameOverSound = false;

		AdMobAndroid.createBanner( AdmobID , AdMobAndroidAd.smartBanner, AdMobAdPlacement.BottomCenter );

	}
	
	// Update is called once per frame
	void Update () {

		if(PlayLevelSound)
		{
			LevelSound();
			PlayLevelSound = false;

		}
		if(PlayGameOverSound)
		{
			GameOverSound();
			PlayGameOverSound = false;
			
		}
		if(PlayPocSound)
		{
			PocSound();
			PlayPocSound=false;
			
		}


		if(!showtap && Ground.transform.position.y > -9 && BooleanaMiscareODataGroundLaStart)
		{
			Ground.transform.position = new Vector3(Ground.transform.position.x,Ground.transform.position.y - moveDownSpeedGroundStart,Ground.transform.position.z);


		}
		if(Ground.transform.position.y <= -9 || Scor>=1)
		{
			BooleanaMiscareODataGroundLaStart = false;
		}

//		playbut.color = 
	
		if(Time.time - TimpDelayPtFade > 1 && StergButonReplay)
			Application.LoadLevel("GamePlayTap");

		if(Scor>PlayerPrefs.GetInt("HighScore"))
		{
			PlayerPrefs.SetInt("HighScore",Scor);
		}

		if(Ground.transform.position.y < this.transform.position.y - 15f)
		{
			Ground.transform.position = new Vector3(Ground.transform.position.x,this.transform.position.y - 15f, Ground.transform.position.z);
		}

		if(MoveCameraOnStart && this.transform.position.y >=1)
		{
			this.transform.position = new Vector3(this.transform.position.x,this.transform.position.y - (2*Time.deltaTime),this.transform.position.z);
		}


		if(Playing)
		{
			Obstacle1.SetActive(true);
			Obstacle2.SetActive(true);
			Obstacle3.SetActive(true);
		}


		if(Ball.transform.position.y > this.transform.position.y || (GameOver && this.transform.position.y>Ground.transform.position.y + 5))
		{
			this.transform.position = new Vector3(this.transform.position.x,Ball.transform.position.y,this.transform.position.z);
		}

		if(GameOver)
		{
			Ball.transform.position = new Vector3(Ball.transform.position.x,Ball.transform.position.y,244.0632f);
		}



	}

	IEnumerator FadeTime(float waitTime) {

	
		i=i-0.1f;
		yield return new WaitForSeconds(0.1f);
		if(i>=0)
		StartCoroutine(FadeTime(0.2f));
	}

	IEnumerator FadeStart(float waitTime) {
		

		i=i+0.1f;
		yield return new WaitForSeconds(0.1f);
		if(i<1)
			StartCoroutine(FadeStart(0.2f));
	}

	public static bool FadeEffectGameOver;
	void OnGUI()
	{
		TitleFont.fontSize = (int)(Screen.width * 0.1225f);
		ScoreMainFont.fontSize = (int)(Screen.width * 0.1225f);
		ScoreFont.fontSize = (int)(Screen.width * 0.05f);
		if(!Playing)
		{


			MainMenu();
		}

		if(Playing && !GameOver)
		{
			BallPhysic();
		}

		if(GameOver && Time.time - TimeDelayGameOver>2.5f)
		{
		
			GameOverScreen();
			if(AdMobApare)
			{

//				AdMobBinding.createBanner( AdmobID, AdMobBannerType.SmartBannerPortrait, AdMobAdPosition.BottomCenter );
				AdMobApare=false;
			}

		}
	}
	bool showtap;
	float moveDownSpeedGroundStart = 0.1f;
	void BallPhysic()
	{
		if(GUI.Button(new Rect(0,0,Screen.width,Screen.height),"", GUIStyle.none))
		{
			BallSound();
			if(!showtap)
			Ball.rigidbody.velocity = new Vector3(0,0,0);
			Ball.rigidbody.AddForce(new Vector3(0,450,0));
			if(showtap)
			{
				Ball.rigidbody.velocity = new Vector3(0,0,0);
				Ball.rigidbody.AddForce(new Vector3(0,600,0));
							}
			showtap = false;
			Ground.transform.position = new Vector3(Ground.transform.position.x,Ground.transform.position.y,Ground.transform.position.z);
			GamePlay.MoveCameraOnStart = false;
		}
		if(showtap)

		GUI.DrawTexture(new Rect(Screen.width * 0.355f,Screen.height * 0.35f,Screen.width * 0.290f,Screen.width * 0.25f),TapTap);
		

		GUI.Label(new Rect(0,Screen.height*0.15f,Screen.width,Screen.height* 0.0f),"" + Scor,ScoreMainFont);
	}
	float i;
	void MainMenu()
	{
		GUI.color=new Color(1f,1f,1f,i);
		GUI.Label(new Rect(0,Screen.height*0.15f,Screen.width,Screen.height* 0.0f),"Jump Ball Up",TitleFont);

		GUI.Label(new Rect(0,Screen.height*0.22f,Screen.width,Screen.height* 0.0f),"BEST: "+ PlayerPrefs.GetInt("HighScore"),ScoreFont);


		if(GUI.Button(new Rect(Screen.width * 0.4f,Screen.height * 0.4f,Screen.width * 0.2f,Screen.width * 0.2f),"",PlayButton))
		{
			if(i>0.9f)
			StartCoroutine(FadeTime(2.0f));


//			MoveCameraOnStart = false;

			ButtonSound();
		}

		if(GUI.Button(new Rect(Screen.width * 0.4f,Screen.height * 0.55f,Screen.width * 0.2f,Screen.width * 0.2f),"",Like))
		{
			Application.OpenURL(URLLink);
			ButtonSound();
		}

//		if(GUI.Button(new Rect(Screen.width * 0.15f,Screen.height * 0.5f,Screen.width * 0.2f,Screen.width * 0.2f),"",Leader))
//		{
//			ButtonSound();
////			SubmitScore();
//
//			// authenticate user:
//			Social.localUser.Authenticate((bool success) => {
//				// handle success or failure
//			});
//			
//			Social.ReportScore(PlayerPrefs.GetInt("HighScore"), GameCenterID , (bool success) => {
//				// handle success or failure
//			});
//			Social.ShowLeaderboardUI();
//		}
		
		
		
		if(i<=0.0f)
		{
			MoveCameraOnStart = false;
			Playing = true;
		}
	}
	public static bool fadeIn = false;
	bool StergButonReplay;
	float TimpDelayPtFade;
	void GameOverScreen()
	{

		if(FadeEffectGameOver)
		{
			i = 0;
			StartCoroutine(FadeStart(2.0f));
			FadeEffectGameOver = false;

		}
		GUI.color=new Color(1f,1f,1f,i);
		if(!StergButonReplay)
		{
			GUI.Label(new Rect(0,Screen.height*0.14f,Screen.width,Screen.height* 0.0f),"Game Over",TitleFont);
			GUI.Label(new Rect(0,Screen.height*0.2f,Screen.width,Screen.height* 0.0f),"SCORE: " + Scor,ScoreFont);
			GUI.Label(new Rect(0,Screen.height*0.25f,Screen.width,Screen.height* 0.0f),"BEST: "+ PlayerPrefs.GetInt("HighScore"),ScoreFont);
		if(GUI.Button(new Rect(Screen.width * 0.4f,Screen.height * 0.35f,Screen.width * 0.2f,Screen.width * 0.2f),"",ReplayButton))
		{
				ButtonSound();
			TimpDelayPtFade = Time.time;
			
			StergButonReplay = true;
			fadeIn = true;

			//Application.LoadLevel(Application.loadedLevel);
		}

//			if(GUI.Button(new Rect(Screen.width * 0.05f,Screen.height * 0.5f,Screen.width * 0.2f,Screen.width * 0.2f),"",FreeGame))
//			{
//				AdColony.ShowVideoAd(AdColonyZoneID);
//				ButtonSound();
//			}

//			if(GUI.Button(new Rect(Screen.width * 0.28f,Screen.height * 0.5f,Screen.width * 0.2f,Screen.width * 0.2f),"",Leader))
//			{
//				ButtonSound();
////				SubmitScore();
//
//				// authenticate user:
//				Social.localUser.Authenticate((bool success) => {
//					// handle success or failure
//				});
//
//				Social.ReportScore(PlayerPrefs.GetInt("HighScore"), GameCenterID, (bool success) => {
//					// handle success or failure
//				});
//				Social.ShowLeaderboardUI();
//				
//			}

			if(GUI.Button(new Rect(Screen.width * 0.4f,Screen.height * 0.5f,Screen.width * 0.2f,Screen.width * 0.2f),"",Like))
			{
				Application.OpenURL(URLLink);
				ButtonSound();
			}
//			if(GUI.Button(new Rect(Screen.width * 0.75f,Screen.height * 0.5f,Screen.width * 0.2f,Screen.width * 0.2f),"",Share))
//			{
//				FB.Login("email,publish_actions", LoginCallback);
//				
//				StartCoroutine(TakeScreenshot());
//				ButtonSound();
//			}



		}

	}


	void ButtonSound(){


		ButtonSource.clip = ButtonClip;
		ButtonSource.Play();
	}
	void LevelSound()
	{
		LevelSource.clip=LevelClip;
		LevelSource.Play();
	}
	void BallSound()
	{
		BallSource.clip=BallClip;
		BallSource.Play();
	}

	void GameOverSound()
	{
		ColliderPocTrigger.SetActive(true);
		GameOverSource.clip=GameOverClip;
		GameOverSource.Play();
	}

	void PocSound()
	{
		PocSource.clip=PocClip;
		PocSource.Play();
	}

	void StartGameCenter()
	{

	}


	void LoadSounds() 
	{
		LevelClip = Resources.Load("Sounds/levelup") as AudioClip;
		ButtonClip = Resources.Load("Sounds/button") as AudioClip;
		GameOverClip = Resources.Load("Sounds/gameover") as AudioClip;
		BallClip = Resources.Load("Sounds/bubble") as AudioClip;
		PocClip = Resources.Load("Sounds/hitdown") as AudioClip;
	}


}
