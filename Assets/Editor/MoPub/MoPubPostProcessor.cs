﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System.IO;
using System.Diagnostics;


public class MoPubPostProcessor : MonoBehaviour
{
	[UnityEditor.MenuItem( "Tools/MoPub/Manually run Xcode post processor...", false, 21 )]
	static void manuallyRunPostProcessor()
	{
		var path = EditorUtility.OpenFilePanel( "Locate your Xcode project file", Application.dataPath.Replace( "/Assets", string.Empty ), "xcodeproj" );
		if( path != null && path.Length > 10 )
		{
#if UNITY_4_6 || UNITY_4_7 || UNITY_4_8
//			onPostProcessBuildPlayer( BuildTarget.iPhone, Directory.GetParent( path ).FullName );
#else
	//		onPostProcessBuildPlayer( BuildTarget.iOS, Directory.GetParent( path ).FullName );
#endif
		}
	}



}
