//
//  ALAdDelegateWrapper.m
//  Unity-iPhone
//
//  Created by Matt Szaro on 1/16/14.
//
//

#import "ALAdDelegateWrapper.h"
#import "ALInterstitialCache.h"
#import "ALErrorCodes.h"

extern void UnitySendMessage(const char *, const char *, const char *);

@implementation ALAdDelegateWrapper
@synthesize isInterstitialShowing, gameObjectToNotify, isIncentReady;

const static NSString* methodName = @"onAppLovinEventReceived";

-(void) callCSharpWithMessage: (NSString*) message
{
    if(gameObjectToNotify)
    {
        UnitySendMessage([gameObjectToNotify cStringUsingEncoding: NSStringEncodingConversionAllowLossy],
                         [methodName cStringUsingEncoding: NSStringEncodingConversionAllowLossy],
                         [message cStringUsingEncoding: NSStringEncodingConversionAllowLossy]
                         );
    }
}

-(void) adService:(ALAdService *)adService didLoadAd:(ALAd *)ad
{
    [ad retain];
    
    if([ad.type isEqual: [ALAdType typeIncentivized]])
    {
        [self callCSharpWithMessage: @"LOADEDREWARDED"];
        self.isIncentReady = YES;
    }
    else
    {
        [self callCSharpWithMessage: [@"LOADED" stringByAppendingString: [[[ad size] label] uppercaseString]]];
    }
}

-(void) adService:(ALAdService *)adService didFailToLoadAdWithError:(int)code
{
    [self callCSharpWithMessage: @"LOADFAILED"];
}

-(void) ad:(ALAd *)ad wasDisplayedIn:(UIView *)view
{
    if([[ad size] isEqual: [ALAdSize sizeInterstitial]])
        isInterstitialShowing = YES;
    
    [self callCSharpWithMessage: [@"DISPLAYED" stringByAppendingString: [[[ad size] label] uppercaseString]]];
}

-(void) ad:(ALAd *)ad wasHiddenIn:(UIView *)view
{
    if([[ad size] isEqual: [ALAdSize sizeInterstitial]])
    {
        isInterstitialShowing = NO;
        [ALInterstitialCache shared].lastAd = nil;
    }
    
    if(ad.type && [ad.type isEqual: [ALAdType typeIncentivized]])
    {
        isIncentReady = NO;
    }
    
    // Fix for Unity 3 releasing object incorrectly
    if(ad.size.label)
    {
        [self callCSharpWithMessage: [@"HIDDEN" stringByAppendingString: [[[ad size] label] uppercaseString]]];
    }
    
    [ad release];
}

-(void) ad:(ALAd *)ad wasClickedIn:(UIView *)view
{
    [self callCSharpWithMessage: @"CLICKED"];
}

-(void) videoPlaybackBeganInAd:(ALAd *)ad
{
    [self callCSharpWithMessage: @"VIDEOBEGAN"];
}

-(void) videoPlaybackEndedInAd:(ALAd *)ad atPlaybackPercent:(NSNumber *)percentPlayed fullyWatched:(BOOL)wasFullyWatched
{
    [self callCSharpWithMessage: @"VIDEOSTOPPED"];
}

-(void) rewardValidationRequestForAd:(ALAd *)ad didSucceedWithResponse:(NSDictionary *)response
{
    [self callCSharpWithMessage: @"REWARDAPPROVED"];

    NSString* amountStr = [[response objectForKey: @"amount"] retain];
    NSString* currencyName = [[response objectForKey: @"currency"] retain];

    if(amountStr && currencyName)
    {
        [self callCSharpWithMessage: [NSString stringWithFormat: @"REWARDAPPROVEDINFO|%@|%@", amountStr, currencyName]];
    }
}

-(void) rewardValidationRequestForAd:(ALAd *)ad didExceedQuotaWithResponse:(NSDictionary *)response
{
    [self callCSharpWithMessage: @"REWARDQUOTAEXCEEDED"];
}

-(void) rewardValidationRequestForAd:(ALAd *)ad wasRejectedWithResponse:(NSDictionary *)response
{
    [self callCSharpWithMessage: @"REWARDREJECTED"];
}

-(void) rewardValidationRequestForAd:(ALAd *)ad didFailWithError:(NSInteger)responseCode
{
    if(responseCode == kALErrorCodeIncentivizedUserClosedVideo)
    {
        [self callCSharpWithMessage: @"USERCLOSEDEARLY"];
    }
    else
    {
        [self callCSharpWithMessage: @"REWARDTIMEOUT"];
    }
}

-(void) userDeclinedToViewAd: (ALAd*) ad
{
    [self callCSharpWithMessage: @"USERDECLINED"];
}

@end
